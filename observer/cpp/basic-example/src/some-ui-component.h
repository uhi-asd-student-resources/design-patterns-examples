#pragma once
#include "observer-interfaces.h"
#include <string>

class FancyUiComponent : public Observer
{
public:
	FancyUiComponent ();

	void update(const std::string& newMessage) override;
	
	void printInfo ();

private:
	
    std::string messageFromSubject;
};