#include "some-ui-component.h"
#include <iostream>

FancyUiComponent::FancyUiComponent ()
{
}

void FancyUiComponent::update (const std::string& newMessage)
{
	messageFromSubject = newMessage;
	printInfo();
}


void FancyUiComponent::printInfo ()
{
	std::cout << "FancyUiComponent \"" << this << "\": a new message is available --> " << messageFromSubject << "\n";
}
