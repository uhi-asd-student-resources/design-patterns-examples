#include "some-ui-component.h"
#include "text-editor.h"

int main (int argc, char* argv[])
{
    TextEditor editor;
	
    FancyUiComponent observer1;
    FancyUiComponent observer2;
    FancyUiComponent observer3;

    editor.attach(&observer1);
    editor.attach(&observer2);
    editor.attach(&observer3);

    editor.onEnterPressed("Some text");
	
    editor.detach(&observer3);

    editor.onEnterPressed("Some other text");

    editor.detach(&observer2);

    editor.onEnterPressed("Some more text");

    editor.detach(&observer1);
	
    return 0;
}
