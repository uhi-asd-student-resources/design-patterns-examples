#pragma once
#include <string>

class Observer
{
public:
    virtual ~Observer() {};
    virtual void update(const std::string& messageFromSubject) = 0;
};

class ObservableObject
{
public:
    virtual ~ObservableObject() {};
    virtual void attach(Observer* observer) = 0;
    virtual void detach(Observer* observer) = 0;
    virtual void notify() = 0;
};