#include "text-editor.h"
#include <iostream>

void TextEditor::attach (Observer* observer)
{
	observers.push_back (observer);
}

void TextEditor::detach (Observer* observer)
{
	observers.remove (observer);
}

void TextEditor::notify ()
{
	for (const auto observer: observers)
	{
		observer->update(textEditorContents);
	}
}

void TextEditor::onEnterPressed (const std::string& message)
{
	textEditorContents = message;
	notify();
}

void TextEditor::updatedFromSavedState ()
{
	textEditorContents = "change message message";
	notify();
}
